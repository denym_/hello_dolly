require 'spec_helper'

describe "HelloDolly" do
	describe "#pick_lyric?" do
		it "should return expected value" do
			HelloDolly.pick_lyric(0).should eq "Hello, Dolly"
		end
	end
end