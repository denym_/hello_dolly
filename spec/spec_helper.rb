require 'rubygems'
require 'bundler/setup'

require 'hello_dolly'

RSpec.configure do |config|
	config.order = "random"
	config.color_enabled = true
	config.tty = true
end